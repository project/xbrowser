<?php
/**
 * @class
 * Class file.
 */

class XbrowserEntityController extends XbrowserControllerBase implements XbrowserControllerInterface {
  protected $hook;

  static function getTypes() {
    $entity_infos = entity_get_info();
    foreach ($entity_infos as $entity_type => $entity_info) {
      if (!isset($entity_info['exportable'])) {
        unset($entity_infos[$entity_type]);
      }
    }
    ksort($entity_infos);
    $keys = array_keys($entity_infos);
    $return = array_combine($keys, $keys);
    return $return;
  }

  public function __construct($type) {
    parent::__construct($type);
    $entity_info = entity_get_info($this->type);
    $this->hook = isset($entity_info['export']['default hook']) ? $entity_info['export']['default hook'] : "default" . $this->type;
  }

  public function getPlugin() {
    return 'xbrowser_entity';
  }

  public function getObjects() {
    $objects = entity_load_multiple_by_name($this->type);
    ksort($objects);
    return $objects;
  }

  public function getObject($name) {
    $object = entity_load_single($this->type, $name);
    return $object;
  }

  protected function export($item) {
    return entity_export($this->type, $item);
  }

  protected function is_valid_object($name) {
    $entity_info = entity_get_info($this->type);
    $is_valid = isset($entity_info['exportable']);
    return $is_valid;
  }

  protected function getDbExport($name) {
    $entity_info = entity_get_info($this->type);
    // @see _entity_defaults_rebuild()
    $status_key = isset($entity_info['entity keys']['status']) ? $entity_info['entity keys']['status'] : 'status';

    $current_object = $this->getObject($name);
    $status = $current_object->$status_key;
    $in_code = $status & ENTITY_IN_CODE;
    $in_db = $status & ENTITY_IN_DB;
    // Entity API always saves in DB, so don't trust $in_db.
    if (TRUE) {
      $db_export = $this->export($current_object);
      return $db_export;
    }
    return NULL;
  }

  protected function getModules() {
    $modules = module_implements($this->hook);
    return $modules;
  }

  /**
   * @param string $name
   * @param string $providing_module
   * @return array
   */
  protected function getDefault($name, $providing_module = '') {
    $cache = &drupal_static('XBrowserEntityController::getDefault');
    if (!isset($cache[$this->type])) {
      $modules = module_implements($this->hook);
      $cache[$this->type][''] = array();
      foreach ($modules as $module) {
        $function = $module . '_' . $this->hook;
        if (function_exists($function)) {
          $cache[$this->type][$module] = array();
          $result = $function();
          if (is_array($result)) {
            $cache[$this->type][$module] = $result + $cache[$this->type][$module];
            $cache[$this->type][''] = $result + $cache[$this->type][''];
          }
        }
      }
    }
    if (isset($cache[$this->type][$providing_module][$name])) {
      $default = $cache[$this->type][$providing_module][$name];
      return $default;
    }
    else {
      $default = NULL;
      return $default;
    }
  }

  /**
   * @param string $name
   * @return array
   */
  protected function getAlterChanges($name) {
    $alter_cache = &drupal_static('XBrowserEntityController::getAlterChanges');

    if (!isset($alter_cache[$this->type])) {
      $default_cache = &drupal_static('XBrowserEntityController::getDefault');
      module_load_include('inc', 'xbrowser', 'xbrowser.alter');

      $collect = function($function, $args, &$result, &$context) {
        $items = $args[0];
        $old_exports = $context;
        $exports = array();
        foreach ($items as $name => $item) {
          $export = $this->export($item);
          $old_export = isset($old_exports[$name]) ? $old_exports[$name] : '';
          if ($function && ($export !== $old_export)) {
            $result[$name][$function] = $export;
          }
          $exports[$name] = $export;
        }
        $context = $exports;
      };

      $last_Defaults = isset($default_cache[$this->type]['']) ? $default_cache[$this->type][''] : array();
      $alter_cache[$this->type] = xbrowser_alter($this->hook, $collect, $last_Defaults);
    }
    $alter_changes = isset($alter_cache[$this->type][$name]) ? $alter_cache[$this->type][$name] : array();
    return $alter_changes;
  }

  public function canResave() {
    return TRUE;
  }

  public function doResave($object) {
    ctools_export_crud_save($this->type, $object);
    entity_save($this->type, $object);
  }
}
